package ordination;

import java.util.*;

public class Patient {
	private String cprnr;
	private String navn;
	private double vaegt;

	//Link til Ordination
	private ArrayList<Ordination> ordinationer; 

	public Patient(String cprnr, String navn, double vaegt) {
		super();
		this.cprnr = cprnr;
		this.navn = navn;
		this.vaegt = vaegt;
		this.ordinationer = new ArrayList<Ordination>();
	}
	public String getCprnr() {
		return cprnr;
	}
	public String getNavn() {
		return navn;
	}
	public void setNavn(String navn) {
		this.navn = navn;
	}
	public double getVaegt(){
		return vaegt;
	}
	public void setVaegt(double vaegt){
		this.vaegt = vaegt;
	}
	public String toString(){
		return navn + "  " + cprnr;
	}
	//Metoder (med specifikation) til at vedligeholde link til Ordination
	public ArrayList<Ordination> getOrdinationer(){
		//laver en ny arraylist for at opretholde oo
		return new ArrayList<Ordination>(ordinationer);
	}
	public void addOrdination(Ordination ordination){
		ordinationer.add(ordination);
	}
	public void removeOrdination(Ordination ordination){
		ordinationer.remove(ordination);
	}
}
