package ordination;

import java.util.Date;
import dateutil.DateUtil;

public abstract class Ordination {
	private Date startDen;
	private Date slutDen;

	private Patient patient;
	
	// Link til Laegemiddel
	private Laegemiddel laegemiddel;

	/**
	 * Laver en ordination med startdato, slutdato og laegemiddel.
	 * Krav: Laegemiddel maa gerne vaere null
	 * Krav: startdato<=slutdato
	 */
	public Ordination(Date startDen, Date slutDen, Patient patient, Laegemiddel laegemiddel){
		this.startDen = startDen;
		this.slutDen = slutDen;
		this.patient = patient;
		this.laegemiddel = laegemiddel;
		patient.addOrdination(this);
	}

	
		
	public Date getStartDen() {
		return startDen;
	}	
	public Date getSlutDen() {
		return slutDen;
	}
	
	/**
	 * Returnerer antal hele dage mellem startdato og slutdato. Begge dage inklusive.
	 * Krav: startdato<=slutdato
	 * @return antal dage ordinationen g�lder for
	 */
	public int antalDage(){
		return DateUtil.daysDiff(startDen, slutDen) + 1;
	}
	public String toString(){
		return DateUtil.dateToString(startDen);
	}
	
	//Metoder(med specifikation) til at vedligeholde link til laegemiddel
	/**
	 * Getter til laegemiddel.
	 * Krav: Laegemiddel maa gerne vaere null.
	 */
	public Laegemiddel getLaegemiddel(){
		return laegemiddel;
	}
	/** Setter til laegemiddel.
	 * Krav: Laegemiddel maa gerne vaere null.
	 */
	public void setLaegemiddel(Laegemiddel laegemiddel){
		this.laegemiddel = laegemiddel;
	}

	/**
	 * Returnerer den totale dosis der er givet i den periode ordinationen er gyldig
	 * @return
	 */
	public abstract double samletDosis();
	
	/**
	 * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen er gyldig
	 * @return
	 */
	public abstract double doegnDosis();
	
	
}
