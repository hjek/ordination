package ordination;

import java.util.*;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser;

	public DagligSkaev(Date startDen, Date slutDen, Patient patient, Laegemiddel laegemiddel, String[] klokkeSlet, double[] antalEnheder){
		super(startDen, slutDen, patient, laegemiddel);
		
		doser = new ArrayList<Dosis>();

		int i = 0;
		while (i<klokkeSlet.length){
			doser.add(new Dosis(klokkeSlet[i], antalEnheder[i]));
			i++;
		}

	}
	
	public void opretDosis(String tid, double antal)
	{
		doser.add(new Dosis(tid,antal));
	}

	//returnerer ny ArrayList for at opretholde oo.
	public ArrayList<Dosis> getDoser(){
		return new ArrayList<Dosis>(doser);
	}

	public double samletDosis(){
		return doegnDosis()*antalDage();
	}

	public double doegnDosis(){
		double doegnDosis=0;
		for (Dosis d:doser){
			doegnDosis+=d.getAntal();
		}
		return doegnDosis;
	}
}
