package ordination;
import java.util.*;

public class PN extends Ordination{

	private double antalEnheder;
	private ArrayList<Date> givetDen;

	public PN(Date startDen, Date slutDen, Patient patient, Laegemiddel laegemiddel, double antal){
		super(startDen, slutDen, patient, laegemiddel);
		antalEnheder = antal;
		givetDen = new ArrayList<Date>();
	}

	
	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen
	 * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(Date givesDen) {
		if (!(givesDen.before(this.getStartDen())||givesDen.after(this.getSlutDen()))){
			givetDen.add(givesDen);
			return true;
		}
		return false;	
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * @return
	 */
	public int getAntalGangeGivet() {
		return givetDen.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public double samletDosis(){
		return antalEnheder * getAntalGangeGivet();
	}

	public double doegnDosis(){
		if (givetDen.size() == 0){
			return 0;
		} else {
			int antalDage = givetDen.size();
			int i = 0;
			while ( i < givetDen.size()-1 ){
				if (givetDen.get(i).equals(givetDen.get(i+1))){
					antalDage--;
				}
				i++;
			}
			return samletDosis()/antalDage;
		}
	}
}
