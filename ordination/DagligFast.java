package ordination;

import java.util.*;

public class DagligFast extends Ordination {
	private Dosis[] doser;

	public DagligFast(Date startDen, Date slutDen, Patient patient, Laegemiddel laegemiddel, double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, patient, laegemiddel);
		doser = new Dosis[4];
		doser[0] = new Dosis("Morgen", morgenAntal);
		doser[1] = new Dosis("Middag", middagAntal);
		doser[2] = new Dosis("Aften", aftenAntal);
		doser[3] = new Dosis("Nat", natAntal);
	}

	//returnerer et nyt array for at opretholde oo.
	public Dosis[] getDoser(){
		return doser.clone();
	}

	public double samletDosis(){
		return doegnDosis()*antalDage();
	}

	public double doegnDosis(){
		double doegnDosis = 0;
		for (Dosis dosis:doser){
			doegnDosis=dosis.getAntal();
		}
		return doegnDosis;
	}
}
