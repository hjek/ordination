package service;


import java.util.Date;
import java.util.List;

import dao.Dao;

import dateutil.DateUtil;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class Service {
	
	/**
	 * Opretter PN ordination
	 * @param startDen
	 * @param slutDen
	 * @param patient
	 * @param laegemiddel
	 * @param antal
	 * @return opretter og returnerer en PN ordination
	 *  Hvis startDato er efter slutDato kastes en RuntimeException og ordinationen oprettes ikke
	 */
	public static PN opretPNOrdination(Date startDen, Date slutDen, Patient patient, Laegemiddel laegemiddel,double antal){
		PN newPN;
		if (startDen.after(slutDen)){
			throw new RuntimeException("Startdato efter slutdato");
			} else {
			newPN = new PN(startDen, slutDen, patient, laegemiddel, antal);
			}
		return newPN;
	}
	
	/**
	 * Opretter DagligFast ordination
	 * @param startDen
	 * @param slutDen
	 * @param patient
	 * @param laegemiddel
	 * @param morgenAntal
	 * @param middagAntal
	 * @param aftenAntal
	 * @param natAntal
	 * @return opretter og returnerer en DagligFast ordination
	 *  Hvis startDato er efter slutDato kastes en RuntimeException og ordinationen oprettes ikke
	 */
	public static DagligFast opretDagligFastOrdination(Date startDen, Date slutDen, Patient patient, Laegemiddel laegemiddel,double morgenAntal, double middagAntal, double aftenAntal, double natAntal){
		DagligFast newDagligFast;
		if (startDen.after(slutDen)){
			throw new RuntimeException("Startdato efter slutdato");
			} else {
			newDagligFast = new DagligFast(startDen, slutDen, patient, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
			}
		return newDagligFast;
	}
	/**
	 * Opretter daglig skaev ordination
	 * Krav: antalEnheder og klokkeSlet har samme laengde. klokkeSlet indeholder tidspunkter paa dagen paa formen 15:30
	 * @param startDen
	 * @param slutDen
	 * @param patient
	 * @param laegemiddel
	 * @param klokkeSlet
	 * @param antalEnheder
	 * @return opretter og returnerer en DagligSkaev ordination
	 *  Hvis startDato er efter slutDato kastes en RuntimeException og ordinationen oprettes ikke
	 */
	public static DagligSkaev opretDagligSkaevOrdination(Date startDen, Date slutDen, Patient patient, Laegemiddel laegemiddel,String[] klokkeSlet, double[] antalEnheder)
	{
		DagligSkaev newDagligSkaev;
		if (klokkeSlet.length != antalEnheder.length){
			throw new RuntimeException("Antal af klokkeslet og enheder er ikke ens");
		}
		for (String k:klokkeSlet){
			if (!(k.matches("([0-1][0-9][^0-9][0-5][0-9][^0-9]?)|(2[0-3][^0-9][0-5][0-9][^0-9]?)"))){
				throw new RuntimeException("Klokkeslet "+k+" er paa forkert form");
				}
		}
		if (startDen.after(slutDen)){
			throw new RuntimeException("Startdato efter slutdato");
			} else {
			newDagligSkaev = new DagligSkaev(startDen, slutDen, patient, laegemiddel, klokkeSlet, antalEnheder);
			}
		return newDagligSkaev;
	}
	
	/**
	 * Registrerer at PN ordinationen er anvendt paa dagen dato
	 * @param ordination
	 * @param dato
	 * En dato for hvornaar ordinationen anvendes tilfoejes ordinationen.
	 * Hvis datoen ikke er indenfor ordinationens gyldighedsperiode kastes en RuntimeException 
	 */
	public static void ordinationPNAnvendt(PN ordination, Date dato){
		if (dato.before(ordination.getStartDen())||dato.after(ordination.getSlutDen())){
			throw new RuntimeException("Dato uden for ordinationens tidsperiode");
		} else {
			ordination.givDosis(dato);
		}
	}
	
	/**
	 * 
	 * @param patient
	 * @param laegemiddel
	 * @return den anbefalede dosis for den paagaeldende patient(der skal tages hensyn til patientens vaegt)
	 * Det er en forskellig enheds faktor der skal anvendes, den er afhaengig af patinetens vaegt 
	 */
	
	public static double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel){
		double result = 0;
		if (patient.getVaegt()<25)
			result = patient.getVaegt()*laegemiddel.getEnhePrKgPrDoegnLet();
		else if (patient.getVaegt()>120)
			result = patient.getVaegt()*laegemiddel.getEnhePrKgPrDoegnTung();
		else 
			result = patient.getVaegt()*laegemiddel.getEnhePrKgPrDoegnNormal();
		return result;
	}
	
	public static List<Patient> getAllPatienter(){
		return Dao.getAllPatienter();
	}
	
	public static List<Laegemiddel> getAllLaegemidler(){
		return Dao.getAllLaegemidler();
	}
	
	public static void createSomeObjects(){
		
		
		Dao.gemPatient(new Patient("Jane Jensen", "121256-0512", 63.4));
		Dao.gemPatient(new Patient("Finn Madsen", "070985-1153", 83.2));
		
		Dao.gemLaegemiddel(new Laegemiddel("Pinex", 0.1,0.15,0.16, "Styk"));
		Dao.gemLaegemiddel(new Laegemiddel("Paracetamol", 1,1.5,2, "Ml"));
		Dao.gemLaegemiddel(new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"));
		Dao.gemLaegemiddel(new Laegemiddel("ABC", 0.01,0.015,0.02, "Styk"));
	
		
//		opretPNOrdination(DateUtil.createDate(2012, 3,1), DateUtil.createDate(2012, 3,12), Dao.getAllPatienter().get(0), Dao.getAllLaegemidler().get(1), 123);
////		opretPNOrdination(DateUtil.createDate(2012, 4,12), DateUtil.createDate(2012, 3,12), Dao.getAllPatienter().get(0), Dao.getAllLaegemidler().get(0), 3);
		
//		opretDagligFastOrdination(DateUtil.createDate(2012, 1,10), DateUtil.createDate(2012, 1,12), Dao.getAllPatienter().get(1), Dao.getAllLaegemidler().get(1),2,-1,1,-1);
//		String[] kl = {"12:00", "12:40","16:00","18:45"};
//		double[] an = {0.5, 1, 2.5, 3};
//		opretDagligSkaevOrdination(DateUtil.createDate(2012, 1,23), DateUtil.createDate(2012, 1,24), Dao.getAllPatienter().get(1), Dao.getAllLaegemidler().get(2), kl, an);
	
	}


}
