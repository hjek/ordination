package gui;

import java.util.Date;

import dateutil.DateUtil;

public class Validering {
	static String[] makeKlokkeSlet(Object[] model) {
		String[] resultat = new String[model.length];
		try {
			for (int i = 0; i < model.length; i++) {
				resultat[i] = ((String) model[i]).substring(0, 6);
			}
		} catch (RuntimeException e) {
			throw new RuntimeException("Klokkeslet er ikke korrekt");
		}
		return resultat;
	}

	static double[] makeAntal(Object[] model) {
		double[] resultat = new double[model.length];
		try {
			for (int i = 0; i < model.length; i++) {
				double dosis = Double.parseDouble(((String) model[i])
						.substring(6));
				resultat[i] = dosis;
			}
		} catch (RuntimeException e) {
			throw new RuntimeException("Der er ikke givet korrekt antal");
		}
		return resultat;
	}

	static Date makeDate(String s) {

		Date date = null;
		try {
			date = DateUtil.createDate(s);
		} catch (RuntimeException e) {
			throw new RuntimeException("Datoen er ikke korrekt format");
		}
		return date;

	}
}
