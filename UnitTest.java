import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;
import dateutil.*;
import ordination.*;
import service.*;


public class UnitTest{
	Ordination o;
	Patient p;
	Laegemiddel l;
	String[] kl;
	double[] stk;
	Date startDen, slutDenOk, slutDenIk;

	@Before
	public void setUp(){
		p = new Patient("041087xxxx", "Pelle Hjek", 90);
		l = new Laegemiddel("Azithromycin", 1, 2, 3, "styk");
		startDen = DateUtil.createDate("2014-01-01");
		slutDenOk = DateUtil.createDate("2014-06-01");
		slutDenIk = DateUtil.createDate("1945-04-09");
	}

	@Test
	public void testOpretDagligFastOrdination1(){
		assertSame(
				Service.opretDagligFastOrdination(
					startDen,slutDenOk,p,l,1,1,1,1),
				p.getOrdinationer().get(0));
	}

	@Test(expected= RuntimeException.class)
	public void testOpretDagligFastOrdination2(){
		Ordination o = Service.opretDagligFastOrdination(
				startDen,slutDenIk,p,l,1,1,1,1);
	}



	@Test
	public void testOpretDagligSkaevOrdination1(){
		assertSame(
				Service.opretDagligSkaevOrdination(
					startDen,slutDenOk,p,l,new String[]{"10:00","12:00"},new double[]{1,1}),
				p.getOrdinationer().get(0));
	}

	@Test(expected= RuntimeException.class)
	public void testOpretDagligSkaevOrdination2(){
		Ordination o = Service.opretDagligSkaevOrdination(
				startDen,slutDenOk,p,l,new String[]{"10:00","12:00"},new double[]{1,1,1});
	}

	@Test(expected= RuntimeException.class)
	public void testOpretDagligSkaevOrdination3(){
		Ordination o = Service.opretDagligSkaevOrdination(
				startDen,slutDenOk,p,l,new String[]{"14:00","80:00"},new double[]{1,1});
	}

	@Test(expected= RuntimeException.class)
	public void testOpretDagligSkaevOrdination4(){
		Ordination o = Service.opretDagligSkaevOrdination(
				startDen,slutDenOk,p,l,new String[]{"100:0","12:00"},new double[]{1,1});
	}

	@Test(expected= RuntimeException.class)
	public void testOpretDagligSkaevOrdination5(){
		Ordination o= Service.opretDagligSkaevOrdination(
				startDen,slutDenIk,p,l,new String[]{"10:00","12:00"},new double[]{1,1});
	}


	@Test
	public void testOpretPNOrdination1(){
		assertSame(
				Service.opretPNOrdination(
					startDen,slutDenOk,p,l,5),
				p.getOrdinationer().get(0));
	}

	@Test(expected= RuntimeException.class)
	public void testOpretPNOrdination2(){
		Ordination o = Service.opretPNOrdination(
				startDen,slutDenIk,p,l,5);
	}




}

